# Copyright 2010 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gtk-doc-am-1.15.ebuild' from Gentoo, which is:
#     Copyright 1999-2010 Gentoo Foundation

MY_PN="gtk-doc"
require gnome.org [ suffix=tar.xz ] python [ blacklist=none multibuild=false ]

SUMMARY="Macros and utilities used by autotools from gtk-doc"
DESCRIPTION="
This package installs macros and utilities from the gtk-doc package that are
used by autotools based build systems that use gtk-doc documentation.
"
HOMEPAGE="https://www.gtk.org/gtk-doc"

LICENCES="FDL-1.1 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS=""

# we don't build anything so we can't run tests
RESTRICT="test"

DEPENDENCIES="
    build:
        app-text/docbook-xml-dtd:4.3
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        dev-python/six[python_abis:*(-)?]
        dev-util/itstool
        virtual/pkg-config[>=0.19]
    build+run:
        !dev-doc/gtk-doc[<=1.15-r1] [[
            description = [ These versions install the same files ]
            resolution = uninstall-blocked-after
        ]]
"

WORK=${WORKBASE}/${MY_PN}-${PV}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/gtk-doc-1.28-backport-rebase_py-fixes.patch
)
DEFAULT_SRC_CONFIGURE_PARAMS=(
    PYTHON=${PYTHON}
)

src_compile() {
    :
}

src_install() {
    dobin gtkdoc-rebase gtkdocize

    insinto /usr/share/aclocal/
    doins gtk-doc.m4

    insinto /usr/share/gtk-doc/data/
    doins gtk-doc{,.flat}.make

    # Install python module required for gtkdoc-rebase
    insinto /usr/share/gtk-doc/python/gtkdoc
    doins gtkdoc/*.py
}

