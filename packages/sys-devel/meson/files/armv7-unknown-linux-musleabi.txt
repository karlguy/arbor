[binaries]
c = 'armv7-unknown-linux-musleabi-cc'
cpp = 'armv7-unknown-linux-muslueabi-c++'
ar = 'armv7-unknown-linux-musleabi-ar'
strip = 'armv7-unknown-linux-musleabi-strip'
pkgconfig = 'armv7-unknown-linux-musleabi-pkg-config'

[host_machine]
system = 'linux'
cpu_family = 'arm'
cpu = 'armv7'
endian = 'little'
