Title: glibc 2.26 update
Author: Thomas Witt <pyromaniac@exherbo.org>
Content-Type: text/plain
Posted: 2017-12-21
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-libs/glibc[>=2.26]

The recent glibc update dropped /usr/host/include/xlocale.h which is used by
perl. This breaks compilation of some perl modules. In order to configure perl
to not use that header, perl itself needs to be recompiled.
